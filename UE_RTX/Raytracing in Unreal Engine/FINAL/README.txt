Title: Real-Time Ray Tracing in Unreal Engine
Author: Vojtech Vavera
File Structure:
- /bin - playable demo of ray tracing in Unreal Engine
- /imgs - images, screenshots
- /latex - source for pdf
- /video - promo video showcasing three benchmarks RTX on/off in UE4

IMPORTANT:
The source files for the demo (UE Project) can be found on GitLab (link below), as combined with the demo itself exceed the upload limit.

https://gitlab.fel.cvut.cz/vavervoj/ueraytracingdemo